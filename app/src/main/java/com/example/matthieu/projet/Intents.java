package com.example.matthieu.projet;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;

/**
 * Created by Alexis on 16/03/2018.
 */

/**
 * The class handling intents.
 */
public class Intents {

    private MainActivity mainActivity;
    private int REQUEST_IMAGE_PICK, REQUEST_IMAGE_CAPTURE;

    /**
     * Constructor
     * @param mainActivity
     * @param pick
     * @param capture
     */
    public Intents(MainActivity mainActivity, int pick, int capture){
        this.mainActivity = mainActivity;
        REQUEST_IMAGE_PICK = pick;
        REQUEST_IMAGE_CAPTURE = capture;
    }

    /**
     * Call the required intent to grab a picture from the gallery, then restart the main activity for result
     */
    public void takeFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mainActivity.startActivityForResult(intent, REQUEST_IMAGE_PICK);
    }

    /**
     * Call the required intent to take a photo, then restart the main activity for result
     */
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mainActivity.getPackageManager()) != null) {
            mainActivity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


}
