package com.example.matthieu.projet;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicConvolve3x3;

/**
 * Created by Alexis on 23/03/2018.
 */

/**
 * The convolution class, which handle both the blur and sharpen algorithm.
 */
public class Convolution {

    private MainActivity m_mainActivity;

    //the matrix used
    private float sharpen_matrix[] = { 0, -1, 0,
            -1, 5, -1,
            0, -1, 0};

    private float blur_matrix[] = new float[]{
            1/16f, 2/16f, 1/16f,
            2/16f, 4/16f, 2/16f,
            1/16f, 2/16f, 1/16f
    };

    public Convolution(MainActivity mainActivity){
        m_mainActivity = mainActivity;
    }

    /**
     * Return a convoluted picture  convoluted by the given matrix
     * @param convolution_matrix = the desired convolution matrix
     * @param source = the image to modify
     * @return the convoluted bitmap
     */
    private Bitmap Convolve(float[] convolution_matrix, Bitmap source){
        Bitmap bmp = create_bitmap_convolve(source, convolution_matrix);
        return bmp;
    }

    public Bitmap Convolve_sharpen(Bitmap source){
        return Convolve(sharpen_matrix, source);
    }

    public Bitmap Convolve_blur(Bitmap source){
        return Convolve( blur_matrix, source);
    }

    private Bitmap create_bitmap_convolve(Bitmap src, float[] matrix){
        Bitmap result = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());

        RenderScript renderScript = RenderScript.create(m_mainActivity);

        Allocation input = Allocation.createFromBitmap(renderScript, src);
        Allocation output = Allocation.createFromBitmap(renderScript, result);

        ScriptIntrinsicConvolve3x3 convolution = ScriptIntrinsicConvolve3x3.create(renderScript, Element.U8_4(renderScript));
        convolution.setInput(input);
        convolution.setCoefficients(matrix);
        convolution.forEach(output);

        output.copyTo(result);

        renderScript.destroy();
        return  result;
    }

}
