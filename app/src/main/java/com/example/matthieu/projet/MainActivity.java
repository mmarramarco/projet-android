package com.example.matthieu.projet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/***
 * The main activity of the application, holding the image the user is modifying, as well as the call to the menu.
 */
public class MainActivity extends AppCompatActivity {
    //private parameters
    CustomImageView targetImage;

    //imageview constants
    static final int MAX_WIDTH = 2048;
    static final int MAX_HEIGHT = 2048;

    //intents constant
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_PICK = 2;

    //key used to restore the current and original pic.
    static String URIKey = "PhotoUri";
    static String OriginalKey = "Original";

    //uri of the current photo and the original.
    Uri photoURI;
    Uri originalURI;

    //call to the other classes.
    Intents m_intent = new Intents(this, REQUEST_IMAGE_PICK, REQUEST_IMAGE_CAPTURE);
    Filter m_filter = new Filter(this);
    Convolution m_convolution = new Convolution(this);

    //Parameters of the menu
    private ListView mDrawerList;

    int brightness;
    double contrast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create the menu, and the items to it.
        mDrawerList = (ListView)findViewById(R.id.navList);
        addDrawerItems();

        //we save the imageview, to access it later.
        targetImage = (CustomImageView)findViewById(R.id.base);

        //we delay the process of putting an image inside it, in case the imageview hasn't been rendered yet.
        targetImage.post(new Runnable() {
            @Override
            public void run() {
                if(photoURI != null)
                    tryPutImage();
            }
        });

        //options for the brightness setting.
        SeekBar seekbarBrightness = (SeekBar)findViewById(R.id.seekBarBrightness);

        seekbarBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                brightness = progress-255;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                targetImage = (CustomImageView) findViewById(R.id.base);
                Bitmap bmp = Filter.doBrightness(((BitmapDrawable)targetImage.getDrawable()).getBitmap(), brightness);
                targetImage.setImageBitmap(bmp);
            }
        });

        //options for the contrast setting
        SeekBar seekbarContrast = (SeekBar)findViewById(R.id.seekBarContrast);

        seekbarContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                contrast = progress;
                if (contrast > 100){
                    contrast = Math.pow((100 + contrast) / 100, 2);
                }
                else if (contrast < 100){
                    contrast /= 100;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                targetImage = (CustomImageView) findViewById(R.id.base);
                Bitmap bmp = m_filter.doContrast(((BitmapDrawable)targetImage.getDrawable()).getBitmap(), contrast);
                targetImage.setImageBitmap(bmp);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(URIKey,photoURI);
        savedInstanceState.putParcelable(OriginalKey,originalURI);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        photoURI = savedInstanceState.getParcelable(URIKey);
        originalURI = savedInstanceState.getParcelable(OriginalKey);
    }

    /**
     * Add each components to the menu, and link the buttons to the corresponding processing functions.
     */
    private void addDrawerItems() {
        String[] osArray = { "Gallery", "Photo", "Net", "Flou","Histogramme","Contraste",
                "Luminosité" ,"Niveau de gris","Sépia","Négatif","Crayon", "Reset","Sauvegarde"};
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //we take the bitmap, then apply a method to it
                Bitmap bmp = ((BitmapDrawable)targetImage.getDrawable()).getBitmap();
                switch (position){
                    case 0:
                        m_intent.takeFromGallery();
                        break;
                    case 1:
                        m_intent.dispatchTakePictureIntent();
                        break;
                    case 2:
                        bmp = m_convolution.Convolve_sharpen(bmp);
                        break;
                    case 3:
                        bmp = m_convolution.Convolve_blur(bmp);
                        break;
                    case 4:
                        bmp = m_filter.histogramEqualization(bmp);
                        break;
                    case 5:
                        findViewById(R.id.seekBarContrast).setVisibility(View.VISIBLE);
                        findViewById(R.id.validateButton).setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        findViewById(R.id.seekBarBrightness).setVisibility(View.VISIBLE);
                        findViewById(R.id.validateButton).setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        bmp = m_filter.toGrayscale(bmp);
                        break;
                    case 8:
                        bmp = m_filter.toSepia(bmp);
                        break;
                    case 9:
                        bmp = m_filter.negative(bmp);
                        break;
                    case 10:
                        bmp = m_filter.sketch(bmp);
                        break;
                    case 11:
                        Reset();
                        break;
                    case 12:
                        Save(bmp);
                        break;
                    default:
                        break;
                }
                //then we update the current URI and put the image back.
                photoURI = getImageUri(getApplicationContext(), bmp);
                tryPutImage();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //did we get something?
        if(resultCode == RESULT_OK){
            //update the URI
            photoURI = data.getData();
            originalURI = photoURI;
            tryPutImage();
        }
    }

    /**
     * Return the complete path to a file knowing the uri
     * @param context = the context of the application
     * @param contentUri = the uri
     * @return the complete path
     */
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Grab the image, and scale it down and rotate it if necessary, then display it.
     */
    private void tryPutImage(){
        // Get the dimensions of the View
        int targetW = targetImage.getWidth();
        int targetH = targetImage.getHeight();

        String path = getRealPathFromURI(getApplicationContext(),photoURI);

        Bitmap source = new BitmapDrawable(this.getResources() , path).getBitmap();
        int currentHeight = source.getHeight();
        int currentWidth = source.getWidth();

        if(currentHeight > MAX_HEIGHT || currentWidth > MAX_WIDTH){


            float scaleWidth = ((float) targetW) / currentWidth;
            float scaleHeight = ((float) targetH) / currentHeight;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            //if the width is greater than the height, we rotate to not have a weird looking image in the end.
            if(currentWidth>currentHeight){
                matrix.postRotate(90);
            }

            // "RECREATE" THE NEW BITMAP
            Bitmap bitmap = Bitmap.createBitmap(
                    source, 0, 0, currentWidth, currentHeight, matrix, false);
            source.recycle();

            targetImage.setImageBitmap(bitmap);
        }
        else{
            targetImage.setImageBitmap(source);
        }

    }

    /**
     * Return the corresponding uri given a bitmap.
     * Known default : this method seems to create a file in order to perform,
     * and will create unecessary duplicata.
     * @param inContext = the context of the application
     * @param source = the source bitmap
     * @return the URI of source.
     */
    public Uri getImageUri(Context inContext, Bitmap source) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        source.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), source, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Save the bitmap on the sdCard.
     * @param bmp the bitmap to save
     */
    private void Save(Bitmap bmp){
        FileOutputStream outStream = null;
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/ModifiedImages");
        dir.mkdirs();
        @SuppressLint("DefaultLocale") String fileName = String.format("%d.jpg", System.currentTimeMillis());
        File outFile = new File(dir, fileName);
        try {
            outStream = new FileOutputStream(outFile);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            outStream.flush();
            outStream.close();
            Toast.makeText(this, "Image saved successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //this part updates the media scanner, making our files appearing immediately in the gallery.
        MediaScannerConnection.scanFile(this, new String[] { outFile.toString() }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }

    /**
     * Reset to the original bitmap. The simpliest method, yet it doesn't work.
     */
    public void Reset(){
        photoURI = originalURI;
        targetImage.setImageURI(originalURI);
    }

    /**
     * hide the bar from the brightness and the contrast options.
     * @param v
     */
    public void hideSeekbar(View v){
        findViewById(R.id.seekBarBrightness).setVisibility(View.GONE);
        findViewById(R.id.seekBarContrast).setVisibility(View.GONE);
        findViewById(R.id.validateButton).setVisibility(View.GONE);
    }



}
