package com.example.matthieu.projet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;

import com.samplersproject.rssample.ScriptC_negativeRS;

import java.nio.IntBuffer;

/**
 * Created by Alexis on 23/03/2018.
 */

/**
 * The class that handle every other filters.
 */
public class Filter {

    private MainActivity m_mainActivity;

    public Filter(MainActivity mainActivity){
        m_mainActivity = mainActivity;
    }

    public Bitmap toSepia(Bitmap color) {
        int red, green, blue, pixel;
        int height = color.getHeight();
        int width = color.getWidth();
        int depth = 20;

        Bitmap sepia = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        int[] pixels = new int[width * height];
        color.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < pixels.length; i++) {
            pixel = pixels[i];

            red = (pixel >> 16) & 0xFF;
            green = (pixel >> 8) & 0xFF;
            blue = pixel & 0xFF;

            red = green = blue = (red + green + blue) / 3;

            red += (depth * 2);
            green += depth;

            if (red > 255)
                red = 255;
            if (green > 255)
                green = 255;
            pixels[i] = (0xFF << 24) | (red << 16) | (green << 8) | blue;
        }
        sepia.setPixels(pixels, 0, width, 0, 0, width, height);
        return sepia;
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {

        Bitmap res = bmpOriginal.copy(bmpOriginal.getConfig(), true);

        // 1) Creer un contexte RenderScript
        android.support.v8.renderscript.RenderScript rs =android.support.v8.renderscript.RenderScript.create(m_mainActivity) ;
        // 2) Creer des Allocations pour passer les donnees
        android.support.v8.renderscript.Allocation input = android.support.v8.renderscript.Allocation.createFromBitmap(rs,res);
        android.support.v8.renderscript.Allocation output = android.support.v8.renderscript.Allocation.createTyped(rs,input.getType());
        // 3) Creer le script
        ScriptC_grey greyScript = new ScriptC_grey(rs);
        // 4) Copier les donnees dans les Allocations
        // ...
        // 5) Initialiser les variables globales potentielles
        // ...
        // 6) Lancer le noyau
        greyScript.forEach_toGrey(input,output);
        // 7) Recuperer les donnees des Allocation (s)
        output.copyTo(res) ;
        // 8) Detruire le context , les Allocation (s) et le script
        input.destroy();
        output.destroy () ;
        greyScript.destroy();
        rs.destroy ();
        return res;
    }

    public Bitmap recolor(Bitmap bmp) {

        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);

        for(int i = 0; i < bmpResult.getWidth(); i++)
        {
            for(int j = 0; j < bmpResult.getHeight(); j++)
            {
                int p = bmpResult.getPixel(i,j);
                int red = Color.red(p);
                int green = Color.green(p);
                int blue = Color.blue(p);
                int alpha = Color.alpha(p);

                if(!(blue < 150 && red > green))
                {
                    red = green = blue = (red+green+blue)/3;
                    bmpResult.setPixel(i,j,Color.argb(alpha, red, green, blue));
                }
            }
        }
        return bmpResult;
    }

    public Bitmap histogramEqualization(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();

        Bitmap res = image.copy(image.getConfig(), true);

        // contexte
        android.support.v8.renderscript.RenderScript rds = android.support.v8.renderscript.RenderScript.create(m_mainActivity);
        // allocations pour passer les données
        android.support.v8.renderscript.Allocation alA = android.support.v8.renderscript.Allocation.createFromBitmap(rds,res);
        android.support.v8.renderscript.Allocation alB = android.support.v8.renderscript.Allocation.createTyped(rds,alA.getType());
        // on crée le script
        ScriptC_histEq histEqScript = new ScriptC_histEq(rds);
        // variables
        histEqScript.set_size(width*height);

        histEqScript.forEach_root(alA, alB);

        histEqScript.invoke_createRemapArray();

        histEqScript.forEach_remaptoRGB(alB, alA);

        alA.copyTo(res);

        alA.destroy();
        alB.destroy();
        histEqScript.destroy();
        rds.destroy();

        return res;
    }

    public static Bitmap doBrightness(Bitmap src, int value) {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;

        // scan through all pixels
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);

                // increase/decrease each channel
                R += value;
                if(R > 255) { R = 255; }
                else if(R < 0) { R = 0; }

                G += value;
                if(G > 255) { G = 255; }
                else if(G < 0) { G = 0; }

                B += value;
                if(B > 255) { B = 255; }
                else if(B < 0) { B = 0; }

                // apply new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }

    public Bitmap doContrast(Bitmap src, double value)
    {
        // image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap

        // create a mutable empty bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());

        // create a canvas so that we can draw the bmOut Bitmap from source bitmap
        Canvas c = new Canvas();
        c.setBitmap(bmOut);

        // draw bitmap to bmOut from src bitmap so we can modify it
        c.drawBitmap(src, 0, 0, new Paint(Color.BLACK));


        // color information
        int A, R, G, B;
        int pixel;
        // get contrast value
        //double contrast = Math.pow((100 + value) / 100, 2);
        double contrast = value;

        // scan through all pixels
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                // apply filter contrast for every channel R, G, B
                R = Color.red(pixel);
                R = (int)(((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(R < 0) { R = 0; }
                else if(R > 255) { R = 255; }

                G = Color.green(pixel);
                G = (int)(((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(G < 0) { G = 0; }
                else if(G > 255) { G = 255; }

                B = Color.blue(pixel);
                B = (int)(((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(B < 0) { B = 0; }
                else if(B > 255) { B = 255; }

                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return bmOut;
    }

    public Bitmap negative(Bitmap image){
        //Create new bitmap
        Bitmap res = image.copy(image.getConfig(), true);

        //Create renderscript
        android.support.v8.renderscript.RenderScript rs = android.support.v8.renderscript.RenderScript.create(m_mainActivity);

        //Create allocation from Bitmap
        android.support.v8.renderscript.Allocation allocationA = android.support.v8.renderscript.Allocation.createFromBitmap(rs, res);

        //Create allocation with same type
        android.support.v8.renderscript.Allocation allocationB = android.support.v8.renderscript.Allocation.createTyped(rs, allocationA.getType());

        //Create script from rs file.
        ScriptC_negativeRS negativeScript = new ScriptC_negativeRS(rs);

        //Call the first kernel.
        negativeScript.forEach_root(allocationA, allocationB);

        //Copy script result into bitmap
        allocationB.copyTo(res);

        //Destroy everything to free memory
        allocationB.destroy();
        allocationA.destroy();
        negativeScript.destroy();
        rs.destroy();

        return res;
    }

    /**
     * Next 2 algos come from https://stackoverflow.com/questions/9826273/photo-image-to-sketch-algorithm/9846701
     **/
    private int colordodge(int in1, int in2) {
        float image = (float)in2;
        float mask = (float)in1;
        return ((int) ((image == 255) ? image:Math.min(255, (((long)mask << 8 ) / (255 - image)))));

    }

    /**
     * Blends 2 bitmaps to one and adds the color dodge blend mode to it.
     */
    public Bitmap ColorDodgeBlend(Bitmap source, Bitmap layer) {
        Bitmap base = source.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap blend = layer.copy(Bitmap.Config.ARGB_8888, false);

        IntBuffer buffBase = IntBuffer.allocate(base.getWidth() * base.getHeight());
        base.copyPixelsToBuffer(buffBase);
        buffBase.rewind();

        IntBuffer buffBlend = IntBuffer.allocate(blend.getWidth() * blend.getHeight());
        blend.copyPixelsToBuffer(buffBlend);
        buffBlend.rewind();

        IntBuffer buffOut = IntBuffer.allocate(base.getWidth() * base.getHeight());
        buffOut.rewind();

        while (buffOut.position() < buffOut.limit()) {
            int filterInt = buffBlend.get();
            int srcInt = buffBase.get();

            int redValueFilter = Color.red(filterInt);
            int greenValueFilter = Color.green(filterInt);
            int blueValueFilter = Color.blue(filterInt);

            int redValueSrc = Color.red(srcInt);
            int greenValueSrc = Color.green(srcInt);
            int blueValueSrc = Color.blue(srcInt);

            int redValueFinal = colordodge(redValueFilter, redValueSrc);
            int greenValueFinal = colordodge(greenValueFilter, greenValueSrc);
            int blueValueFinal = colordodge(blueValueFilter, blueValueSrc);

            int pixel = Color.argb(255, redValueFinal, greenValueFinal, blueValueFinal);

            buffOut.put(pixel);
        }

        buffOut.rewind();

        base.copyPixelsFromBuffer(buffOut);
        blend.recycle();

        return base;
    }

    public Bitmap sketch(Bitmap image){
        Bitmap grey = toGrayscale(image);
        Bitmap neg = negative(grey);
        Convolution m_convolution = new Convolution(m_mainActivity);
        Bitmap blur = m_convolution.Convolve_blur(neg);
        return ColorDodgeBlend(blur, grey);
    }

}
