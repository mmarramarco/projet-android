#pragma version(1)
#pragma rs_fp_relaxed
#pragma rs java_package_name(com.samplersproject.rssample)

#include "rs_debug.rsh"

uchar4 RS_KERNEL root(uchar4 in, uint32_t x, uint32_t y) {
//Convert input uchar4 to float4
  float4 f4 = rsUnpackColor8888(in);
  float red = 1.0f - f4.r;
  float green = 1.0f - f4.g;
  float blue = 1.0f - f4.b;
  return rsPackColorTo8888(red, green, blue, 1.0f);
}